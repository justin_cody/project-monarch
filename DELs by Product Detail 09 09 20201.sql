---DROP table IF EXISTS SAND_CAST.ACTIVITIES_DEL_WO;
--CREATE table SAND_CAST.ACTIVITIES_DEL_WO AS
---fulfill_type ,legacy_wo, wo_found,wo_where_found
select 
case when performance = 'DEL Expectation Met' then 1 else 0 end Met
,a.*
from (select
case when fulfill_type = 'COU' AND Del_bet_4pm_and_959am = 'Y' THEN 1 ELSE 0 END After_Hours_COU_usage,
case when fulfill_type = 'COU' THEN 1 ELSE 0  END COU_Usage,
EXTRACT (HOUR from LTZdtReleased) Hour_Release,
case when EXTRACT(HOUR from LTZdtReleased) <= 0 then 'Released after Customer Expectation'
	 when hrsNBDtoClose = 0 then 'DEL Expectation Met' else 'DEL Expectation Not Met' 
	 end Performance,
a.*
from (
select
case 
	when  rop_del_asset_from_dept = 7 AND wo_type_code IN ('DEL','PCK') 
	THEN 'STO'
	ELSE 
	CASE
		when (      (svmxc__order_type__c = 'DEL' AND 
					(Waybill_Number__c not like '%1Z%' or Waybill_Number__c is null or Waybill_Number__c = 'None')
					 and (Rop_waybill_number not like '%1Z%' or rop_waybill_number is null))
					AND 
					   (SVMXC__Special_Instructions__c not like '%1Z%' or SVMXC__Special_Instructions__c  is null)
															and 
					   (rop_frt_carrier_code in ('OTH','VEL')   OR Freight_Carrier__c  = 'Other Courier' OR 
						assigned_to_first_name like '%COURIER%' OR assigned_to_last_name like '%COURIER%' OR
						technician_First_name  like '%COURIER%' OR technician_Last_name  like '%COURIER%' OR
						upper(assigned_to_first_name)  like '%COUR%' OR upper(assigned_to_first_name) like '%DYNA%' OR
						upper(svmxc__special_instructions__c) like '%COUR%' OR upper(wo_special_instructions)       like '%COUR%' 
					   )
				)
			   THEN 'COU'
			   ELSE 
			   CASE
					when ( (rop_del_asset_from_dept <> 7 or rop_del_asset_from_dept is null)  AND 
									wo_type_code IN ('DEL','PCK') and 
							    (rop_ser_freight_carrier_code in ('UPS','FEX','FEG','AEX')  
								   or rop_ser_freight_carrier_code not in ('OTH','VEL') ) 
								  and rop_waybill_number Like '%1Z%' ) or 
								  (Waybill_Number__c like '%1Z%' or SVMXC__Special_Instructions__c like '%1Z%')
								  THEN 'UPS'
								  ELSE 
								   case when (upper(technician_First_name)  like '%SALES%' OR UPPER(technician_Last_name)  like '%SALES%'   OR
								   			  UPPER(assigned_to_first_name) like '%SALES%' OR UPPER(assigned_to_last_name) like '%SALES%')
								   				OR 
								   			 (UPPER(SVMXC__Special_Instructions__c) like '%TRUNK%'or upper(wo_special_instructions) like '%TRUNK%')
								   		   then 'SAL' 
										   else
										    CASE
											    when ( (rop_del_asset_from_dept <> 7 or rop_del_asset_from_dept is null)  AND 
															wo_type_code IN ('DEL') and rop_waybill_number Like '%1Z%')
															or
												((Waybill_Number__c not like '%1Z%' or Waybill_Number__c is null ) AND 
											      (SVMXC__Special_Instructions__c not like '%1Z%' or SVMXC__Special_Instructions__c is null
											       or SVMXC__Special_Instructions__c  NOT like '%DYNA%'
											       or SVMXC__Special_Instructions__c  NOT like '%COUR%') and  
											     Freight_Carrier__c <> 'Other Courier' and technician_First_name not like '%COURIER%') 
											     THEN 'SVC'
											     
											     ELSE 'SVC'
										     end
								  END
					 eND
	END 	
END fulfill_type,
CASE 
when hrsNBDtoClose <=0 THEN  'DEL Expectation Met'
when hrsNBDtoClose <=1 THEN  '0-1 hrs'
when hrsNBDtoClose <=2 THEN  '1-2 hrs'
when hrsNBDtoClose <=3 THEN  '2-3 hrs'
when hrsNBDtoClose <=4 THEN  '3-4 hrs'
when hrsNBDtoClose <=8 THEN  '4-8 hrs'
when hrsNBDtoClose <=12 THEN  '8-12 hrs'
when hrsNBDtoClose <=18 THEN  '12-18 hrs'
when hrsNBDtoClose <=24 THEN  '18-24 hrs' ELSE '24+ hrs' END DEL_Performance,
cast(from_unixtime(UNIX_TIMESTAMP(LTZdtClosed), 'HH:mm:ss') as string) temp_time,
case when Service_center_site <> vdc_serv_SVC then 'Y' else 'N' end VDC_Serv_Y_N,
a.*
from (SELECT 
substring(split_part(cast(trunc(svmxc__closed_on__c,'MONTH') as varchar(30)),' ',1),1,7) year_month_closed,
case when d.week_overall_num >= (d2.week_overall_num - 13) then "T13W"
            when d.week_overall_num >= (d2.week_overall_num - 26) then "T26W"
            when d.week_overall_num >= (d2.week_overall_num - 56) then "T56W"
           													    else "Prior History" end  DateRange
,case when ip.svmxc__product_name__c is not null then ip.svmxc__product_name__c else gp.prd_description end Product_Name
,TRIM(split_part(L.NAME,'-',1)) Service_center
,TRIM(split_part(L.NAME,'-',2)) Service_center_site
,gs2.cst_accnt_type_code Ship_to_type_code,a.site_id__c shiptocustomerid
--,split_part(so.account_name__c,'-',1) 
,case when (a_wo_caller_company <> 'null')  and (a_wo_caller_company is not null)   then a_wo_caller_company
	  when (wo_caller_company <> 'null')    and(wo_caller_company is not null)      then wo_caller_company
	  when (wo2.wo_dropship_name<> 'null')	and (wo2.wo_dropship_name is not null)	then wo2.wo_dropship_name
	  when (wo.a_wo_dropship_name<> 'null')	and (wo.a_wo_dropship_name is not null)	then wo.a_wo_dropship_name
	  else ''	
 end  ship_to_customer_name
,a.district__c,so.svmx_dispatch_region__c,so.svmxc__country__c
,l.regional_manager_name__c,l.customer_service_manager_name_formula__c,so.svmxc__order_type__c, '' "fullfilment_method"
,case when wo.a_wo_dropship_account_no <> 'null' or wo2.wo_dropship_account_no <> 'null' then 'Y' else 'N' end Transition
,CAST(so.legacy_id__c AS INT) legacy_wo,
case when cast(coalesce(gs.cst_marketing_segment_code,gs2.cst_marketing_segment_code) as int)
			 IN ( 1, 2, 4 , 5, 6, 40, 89, 17, 20) then 'ACUTE' ELSE '' END segment,
rop_del_asset_from_dept, coalesce(WO.A_WO_STATUS_CODE ,WO2.WO_STATUS_CODE) wo_status_code,
coalesce(wo.a_wo_modified_time_stamp,wo2.wo_modified_time_stamp) wo_modified_time_stamp,
decode(wo_type_code, "OTH", wo_other_type_code,wo_type_code) wo_type_code,
grop.rop_ser_freight_carrier_code,rop_waybill_number,ge.EMP_TITLE_CODE,WSA.WSA_TM_METHOD_CODE,
coalesce(wo.a_WO_ASSIGNED_EMP_FK,wo2.WO_ASSIGNED_EMP_FK) WO_ASSIGNED_EMP_FK ,Waybill_Number__c,
svmxc__special_instructions__c,Freight_Carrier__c,ge2.EMP_FIRST_NAME technician_First_name, 
ge2.EMP_Last_NAME technician_Last_name, so.technician_employee_id__c,rop_frt_carrier_code,
ge.emp_first_name assigned_to_first_name,ge.emp_last_name assigned_to_last_name,
coalesce(wo2.wo_special_instructions,wo.a_wo_special_instructions) wo_special_instructions, 
so.method_code__c,
case when coalesce(wo.a_wo_cst_fk,wo2.wo_cst_fk) is not null then 'found' else 'nope' end wo_found,
case when wo2.wo_pk is not null then 'found in hero.genesis_work_order' 
	 when  wo.a_wo_pk is not null then 'found in hero.genesis_archive_work_order'  else ' nope' end wo_where_found,
so.account_name__c account_name,ip.account_market_segment__c,so.account_site_id__c ,so.deliver_to_account_number__c,
so. ship_state__c,
case when 
((extract(delivery_date__c,'HOUR')* 60) + extract(delivery_date__c,'MINUTE')) >= 960 or 
((extract(delivery_date__c,'HOUR')* 60) + extract(delivery_date__c,'MINUTE')) < 600 then 'Y' else 'N'
end Del_bet_4pm_and_959am,FROM_UNIXTIME(UNIX_TIMESTAMP(delivery_date__c),  'HH') Delivery_hour,
case when rop_start_bill_emp_fk = 'AUTOAP' then 'Y' else 'N' end Auto_Generated,
wo_avail_for_ship_date Available_For_Ship_Date,concat(ce.EMP_Last_NAME,',',ce.EMP_FIRST_NAME) clsd_by,
case when
((extract(svmxc__closed_on__c,'HOUR')* 60) + extract(svmxc__closed_on__c,'MINUTE')) <= 1020 and 
((extract(svmxc__closed_on__c,'HOUR')* 60) + extract(svmxc__closed_on__c,'MINUTE')) >= 540 then 'Business Hours' else 'After Hours'
end Hours_type_close,
from_unixtime(unix_timestamp(TO_DATE(svmxc__closed_on__c), 'yyyy-MM-dd'), 'MM/dd/yyyy') closed_date_trun,
case when l.consignment__c  = 'True' then 1 else 0 end Consignment
,concat(ecr.last_name__c,',',ecr.first_name__c) Created_By,so.account_number_hha_id__c Customer_number
,case when so.svmxc__order_type__c = 'DEL' and so.market_segment__c = 'HOM' then 'Inappropriate' else 'Appropriate' end DEL_2_HOM,
DATEDIFF(svmxc__closed_on__c,so.createddate)*24 hrsCreatetoClose,
DATEDIFF(so.need_by_date__c,so.createddate)*24 hrsCreatetoNBD,
DATEDIFF(so.need_by_date__c,so.createddate)*24 > 4 hrsCreatetoNBDGRTR4Hrs,
DATEDIFF(shipped_date__c,so.createddate)*24 hrsCreatetoRelease,
DATEDIFF(so.need_by_date__c,svmxc__closed_on__c)*24 hrsNBDtoClose,
DATEDIFF(so.need_by_date__c,svmxc__closed_on__c)*24 > 4 hrsNBDtoCloseGRTR4Hrs,
DATEDIFF(shipped_date__c ,svmxc__closed_on__c)*24 hrsReleasetoClose,	
DATEDIFF(shipped_date__c ,so.need_by_date__c)*24 hrsReleasetoNBD,
case 
	when l.timezone__c = 'Eastern' then HOURS_ADD(svmxc__closed_on__c,1)
	when l.timezone__c = 'Pacific' then HOURS_ADD(svmxc__closed_on__c,-2)
	when l.timezone__c = 'Central' then HOURS_ADD(svmxc__closed_on__c,0)
	when l.timezone__c = 'Hawaiian' then HOURS_ADD(svmxc__closed_on__c,-5)
	when l.timezone__c = 'Alaskan' then HOURS_ADD(svmxc__closed_on__c,-3)
	when l.timezone__c = 'Mountain' then HOURS_ADD(svmxc__closed_on__c,-1)
end LTZdtClosed,
case 
	when l.timezone__c = 'Eastern' then HOURS_ADD(so.createddate,1)
	when l.timezone__c = 'Pacific' then HOURS_ADD(so.createddate,-2)
	when l.timezone__c = 'Central' then HOURS_ADD(so.createddate,0)
	when l.timezone__c = 'Hawaiian' then HOURS_ADD(so.createddate,-5)
	when l.timezone__c = 'Alaskan' then HOURS_ADD(so.createddate,-3)
	when l.timezone__c = 'Mountain' then HOURS_ADD(so.createddate,-1)
end LTZdtCreated,
case 
	when l.timezone__c = 'Eastern' then HOURS_ADD(so.need_by_date__c,1)
	when l.timezone__c = 'Pacific' then HOURS_ADD(so.need_by_date__c,-2)
	when l.timezone__c = 'Central' then HOURS_ADD(so.need_by_date__c,0)
	when l.timezone__c = 'Hawaiian' then HOURS_ADD(so.need_by_date__c,-5)
	when l.timezone__c = 'Alaskan' then HOURS_ADD(so.need_by_date__c,-3)
	when l.timezone__c = 'Mountain' then HOURS_ADD(so.need_by_date__c,-1)
end LTZdtNBD,
case 
	when l.timezone__c = 'Eastern' then HOURS_ADD(grop.rop_avail_for_ship_date,1)
	when l.timezone__c = 'Pacific' then HOURS_ADD(grop.rop_avail_for_ship_date,-2)
	when l.timezone__c = 'Central' then HOURS_ADD(grop.rop_avail_for_ship_date,0)
	when l.timezone__c = 'Hawaiian' then HOURS_ADD(grop.rop_avail_for_ship_date,-5)
	when l.timezone__c = 'Alaskan' then HOURS_ADD(grop.rop_avail_for_ship_date,-3)
	when l.timezone__c = 'Mountain' then HOURS_ADD(grop.rop_avail_for_ship_date,-1)
end LTZdtReleased,so.dept__c Dept,
case when so.proof_doc_created__c = 'True' then 1 else 0 end Document_Created,
case when wo_dropship_account_no = 'null'  then 0 else 1 end Dropship,
so.free_reason_code__c Free_Reason_Code,so.free_reason_code__c Free_Reason_Description,
rop_del_asset_from_dept from_dept,gsa.svc_description,
case when 
((extract(svmxc__closed_on__c,'HOUR')* 60) + extract(svmxc__closed_on__c,'MINUTE')) >= 960 or 
((extract(svmxc__closed_on__c,'HOUR')* 60) + extract(svmxc__closed_on__c,'MINUTE')) < 480
then 'After Hours' else 'Business Hours' end hour_type,
extract(so.svmxc__closed_on__c,'MONTH') Month_Closed,
coalesce(wo_order_source,a_wo_order_source) Order_source,so. proof_doc_created__c,
so.pouu_detail__c PSA_Reason,a.ready_care__c Ready_Care_Customer,
case when ip.assigned_retrofit__c is null or ip.assigned_retrofit__c = 'None' then 'N' else 'Y' end Retrofit,
case when DATEDIFF(so.need_by_date__c,so.createddate) <=0 then 'Y' else 'N' end Same_day,
case 
when DATEDIFF(so.need_by_date__c,so.createddate) <4 then '<4 hrs' 
when DATEDIFF(so.need_by_date__c,so.createddate) between 4 and 8 then '4-8 hrs' 
when DATEDIFF(so.need_by_date__c,so.createddate) between 8 and 12 then '8-12 hrs' 
when DATEDIFF(so.need_by_date__c,so.createddate) between 12 and 15 then '15-18 hrs' 
     else '+18 hrs'
end Same_day_Bracket,
ip.name Serial_Number,ip.last_in_storage_days__c ser_Last_in_Storage_days,
so.ship_address_1__c Shipt_to_Customer,so.ship_city__c Ship_to_City,
so.ship_state__c Ship_to_State,so.ship_zip_code__c Ship_to_Zip,
so.market_segment_text__c Market_segment,l.service_size__c svc_size,
so.therapy_hours__c Therapy_hours,so.therapy_start_time__c Therapy_Start_Time,	
so.need_by_date__c time_NBD, transport_method__c Transport_Method,
case when last_in_storage_days__c < 150 then  'YES' ELSE 'NO' end Under_150,
so.ups_shipment_record_present__c UPS_confirm,
case when vdc__c  = 'True' then TRIM(split_part(l.NAME,'-',1)) else '' end vdc_serv_SVC,
case when vdc__c  = 'True' then TRIM(split_part(l.NAME,'-',2)) else '' end vdc_serv_SVC_Name,
case when vdc__c  = 'True' then 'VDC' else 'non-vdc' end VDC_TYPE,
SO.pickup_waybill__c waybill_number_pickup,
trunc(svmxc__closed_on__c,'day') Week_of_Closed_Date,
coalesce(wo_avail_for_ship_date,a_wo_avail_for_ship_date) Wo_Avail_Service_Date,
extract(svmxc__closed_on__c,'YEAR') Year_Closed,
concat(cast(extract(svmxc__closed_on__c,'YEAR') as string),cast(WEEKOFYEAR(svmxc__closed_on__c) as string)) Year_Week_Closed,
SO.wo_qty__c WO_QTY
from  servicemax1.svmxc__service_order__c so 
	left join servicemax1.svmxc__installed_product__c IP on so.svmxc__component__c = IP.id
	left join servicemax1.account a on so.svmxc__company__c = a.id
	left join servicemax1.svmxc__site__c l on so.kci_service_center_location__c = l.id
	left join servicemax1.SVMXC__Service_Group_Members__c u on so.SVMXC__Group_Member__c = u.id
	left join hero.genesis_archive_work_order wo on wo.a_wo_pk = CAST(so.legacy_id__c AS INT)
	left join hero.genesis_work_order wo2 on wo2.wo_pk = CAST(so.legacy_id__c AS INT)
	left join (select accountnumber,split_part(a.name,'-',1) account_name from servicemax1.account 
				a group by 1,2) aa on aa.accountnumber =
				coalesce(wo.a_wo_dropship_account_no,wo2.wo_dropship_account_no,'999999')
	left join hero.genesis_service_protocol sp on sp.sp_cst_fk = case when wo.a_wo_cst_fk is null then case when wo2.wo_cst_fk is not null then wo2.wo_cst_fk else -999 end else wo.a_wo_cst_fk end
	left join hero.genesis_product gp on gp.prd_product_pk = wo2.wo_product_fk 
	left join hero.genesis_customer gs on gs.cst_pk = case when wo.a_wo_cst_fk is not null 
																then wo.a_wo_cst_fk 
																else case 
																	 when wo2.wo_cst_fk is not null
																	      then wo2.wo_cst_fk 
																	      else -9999090
																	 end 
													  end
	left join hero.genesis_customer gs2 on gs2.cst_pk = case when wo.a_WO_CALLER_CST_FK is not null 
																then wo.a_WO_CALLER_CST_FK 
																else case 
																	 when wo2.WO_CALLER_CST_FK is not null
																	      then wo2.WO_CALLER_CST_FK
																	      else -9999090
																	 end 
														end
left join (select week_bgn_date,week_overall_num from aim.aim_monthday_dim where week_bgn_date > '2021-01-01 00:00:00'
                                                      and week_bgn_date < now() group by 1, 2) d on  so.week_beginning__c = d.week_bgn_date	
left join (select "False" match,week_overall_num from aim.aim_monthday_dim where monthday_key = trunc(now(), "D") group by 1, 2) d2 on so.isdeleted = d2.match
left join hero.genesis_rental_order_product grop on grop.rop_pk = COALESCE (wo.a_wo_rop_fk,wo2.wo_rop_fk)
left join hero.GENESIS_EMPLOYEE ge on ge.emp_pk = COALESCE (wo.a_wo_assigned_emp_fk,wo2.wo_assigned_emp_fk)
left join hero.GENESIS_EMPLOYEE ge2 on ge2.emp_pk = so.technician_employee_id__c
left join hero.GENESIS_EMPLOYEE CE ON CE.emp_pk = coalesce(wo.a_wo_closed_emp_fk,wo2.wo_closed_emp_fk)
left join servicemax1.employee__c eCR on cast(eCR.name as int) = cast(so.created_by_employee_id__c as int)
left join hero.GENESIS_WO_STORAGE_ASSETS WSA ON WSA.WSA_WO_FK = CAST(so.legacy_id__c AS INT)
left join hero.genesis_service_center gsa on gsa.svc_pk = TRIM(split_part(L.NAME,'-',1)) 
where (
	  (wo2.WO_TRANSITION_GENERATED_FLAG = 'null' and  wo.A_WO_TRANSITION_GENERATED_FLAG = 'null') 
	  and 
	  (wo.a_WO_TRANSPORT_METHOD_CODE Not In ( 'DDC','PAD','CPU' ) and wo2.WO_TRANSPORT_METHOD_CODE Not In ( 'DDC','PAD','CPU' ))
	  and 
	  ((WO.A_WO_PRODUCT_FK LIKE 'WND%'OR WO.A_WO_PRODUCT_FK LIKE 'VAC%'OR WO.A_WO_PRODUCT_FK LIKE 'CLT%') or
	  (WO2.WO_PRODUCT_FK LIKE 'WND%'OR WO2.WO_PRODUCT_FK LIKE 'VAC%'OR WO2.WO_PRODUCT_FK LIKE 'CLT%'))
	  AND 
	  (WO.A_WO_STATUS_CODE = 'CLO' OR WO2.WO_STATUS_CODE = 'CLO' )
	     )
	  AND trunc(svmxc__closed_on__c,'MONTH') >= '2020-08-01 00:00:00'
	 and (ip.svmxc__product_name__c is not null	or gp.prd_description is not null)
	 group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,	15,16,17,18,19,20,21,22,23,24,25,26,27
	 ,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49
	 ,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74
	 ,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100
	 ,101,102,103,104,105,106,107)a 
	 where svmxc__order_type__c = 'DEL') a) a;
/*DROP VIEW  IF EXISTS SAND_CAST.ACTIVITIES_DEL_WO_VW;
CREATE VIEW SAND_CAST.ACTIVITIES_DEL_WO_VW AS
select 
daterange,year_month_closed,product_name,service_center,service_center_site,cst_accnt_type_code,shiptocustomerid
,ship_to_customer_name,district__c,svmx_dispatch_region__c,svmxc__country__c
,regional_manager_name__c,	customer_service_manager_name_formula__c,	svmxc__order_type__c
,	fullfilment_method,	transition,	wo_qty
from SAND_CAST.ACTIVITIES_DEL_WO;*/